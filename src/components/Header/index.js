import React from "react";
import styles from "./styles";
import Link from "next/link";
import { useRouter } from "next/router";

export default function Header() {
  const router = useRouter();
  return (
    <>
      <style jsx>{styles}</style>
      <header className="header-component">
        <Link href="/pedidos-compra">
          <span
            className={router.pathname === "/pedidos-compra" ? "active" : ""}
          >
            Pedidos de Compra
          </span>
        </Link>
        <Link href="/pedidos-troca">
          <span
            className={router.pathname === "/pedidos-troca" ? "active" : ""}
          >
            Pedidos de Troca
          </span>
        </Link>
      </header>
    </>
  );
}
