import css from "styled-jsx/css";

export default css`
  .header-component {
    display: flex;
    align-items: center;
    gap: 20px;
    background-color: var(--color-primary);
    height: 9vh;
    padding: 0 20px;
  }

  .header-component span {
    font-size: 1.2rem;
    color: var(--color-base);
  }

  .header-component span.active {
    border-bottom: 3px solid var(--color-base);
  }
`;
