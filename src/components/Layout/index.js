import React from "react";
import styles from "./styles";

export default function Layout({ children }) {
  return (
    <div>
      <style jsx>{styles}</style>
      {children}
    </div>
  );
}
