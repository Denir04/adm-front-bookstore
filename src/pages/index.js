import React, { useEffect } from "react";
import { useRouter } from "next/router";

function Index() {
  const router = useRouter();
  useEffect(() => {
    router.replace("pedidos-compra");
  }, []);
  return <></>;
}

export default Index;
