# ADM - BookStore

- Aplicação feita em React 18 e Next 13
- Estilos em Styled JSX
- Arquitetura de pastas ITCSS adaptado ao React

## Para instalar dependências

> npm install

## Para Rodar

> npm run dev
